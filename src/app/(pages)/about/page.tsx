import React from 'react';
import type { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'About',
  description: 'About page description',
};

const Page = () => {
  return <div>About page</div>;
};

export default Page;
