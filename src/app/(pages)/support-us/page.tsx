import React from 'react';
import type { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Support us',
  description: 'Support us page description',
};
const Page = () => {
  return <div>Support us page</div>;
};

export default Page;
