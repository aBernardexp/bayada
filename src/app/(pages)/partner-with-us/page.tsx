import React from 'react';
import type { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Partner with us',
  description: 'Partner with us page description',
};
const Page = () => {
  return <div>Partner with us page</div>;
};

export default Page;
