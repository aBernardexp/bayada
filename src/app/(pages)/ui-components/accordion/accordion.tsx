import React, { useState } from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import { createTheme } from '@mui/material/styles';
import { ThemeProvider } from '@emotion/react';
import Icon from '../icon/Icon';

const darkAccordionTheme = createTheme({
  components: {
    MuiAccordionSummary: {
      styleOverrides: {
        root: {
          backgroundColor: '#f6f7f7',
          boxShadow: 'none',
          '&.Mui-expanded': {
            marginBottom: 0,
            background: '#f6f7f7',
          },
          '&.MuiAccordionSummary-root': {
            background: '#f6f7f7',
          },
          '&.MuiPaper-root': {
            boxShadow: 'none',
          },
        },
        content: {
          '&.Mui-expanded': {
            marginBottom: 1,
          },
        },
      },
    },
  },
});

export function LightAccordion() {
  const [isExpanded, setExpanded] = useState(false);

  const handleToggle = () => {
    setExpanded(!isExpanded);
  };

  return (
    <>
      <Accordion
        onClick={() => {
          handleToggle();
        }}
      >
        <AccordionSummary
          className='accordion-summary'
          expandIcon={
            <>
              {
                <Icon
                  iconName={isExpanded ? 'minus' : 'plus'}
                  className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
                />
              }
            </>
          }
          aria-controls='panel1a-content'
          id='panel1a-header'
        >
          <div className='flex items-center'>
            <div className='accordion-icon mr-2 md:mr-4'>
              <Icon
                iconName='person'
                className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
              />
            </div>
            <h6 className='t-16-17 font-frutiger font-bold'>
              Adult and Private Duty Nursing
            </h6>
          </div>
        </AccordionSummary>
        <AccordionDetails className='accordion-details'>
          <div className='-mt-4 pl-5'>
            <p className='t-14-15 pl-5'>
              We provide long-term nursing care to individuals, including those
              needing tracheostomy and ventilator care. BAYADAbility Rehab
              Solutions delivers specialized care for adults with ALS, MS, or
              catastrophic diagnoses such as spinal cord and traumatic brain
              injuries.
            </p>
          </div>
        </AccordionDetails>
      </Accordion>
    </>
  );
}

export const DarkAccordion = () => {
  const [isExpanded, setExpanded] = useState(false);

  const handleToggle = () => {
    setExpanded(!isExpanded);
  };

  return (
    <ThemeProvider theme={darkAccordionTheme}>
      <Accordion
        onClick={() => {
          handleToggle();
        }}
      >
        <AccordionSummary
          className='accordion-summary'
          expandIcon={
            <>
              {
                <Icon
                  iconName={isExpanded ? 'minus' : 'plus'}
                  className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
                />
              }
            </>
          }
          aria-controls='panel1a-content'
          id='panel1a-header'
        >
          <div className='flex items-center'>
            <div className='accordion-icon mr-2 md:mr-4'>
              <Icon
                iconName='person'
                className='svg-icon icon-24 moveRightIcon flex items-center justify-center'
              />
            </div>
            <h6 className='t-16-17 font-frutiger font-bold'>
              Adult and Private Duty Nursing
            </h6>
          </div>
        </AccordionSummary>
        <AccordionDetails className='accordion-details position-relative'>
          <div className='pl-5'>
            <p className='t-14-15 pl-5'>
              We provide long-term nursing care to individuals, including those
              needing tracheostomy and ventilator care. BAYADAbility Rehab
              Solutions delivers specialized care for adults with ALS, MS, or
              catastrophic diagnoses such as spinal cord and traumatic brain
              injuries.
            </p>
          </div>
        </AccordionDetails>
      </Accordion>
    </ThemeProvider>
  );
};
