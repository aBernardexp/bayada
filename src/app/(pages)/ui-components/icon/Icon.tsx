import React from 'react';

interface CustomIconProps {
  iconName: string;
  className?: string;
}

const Icon: React.FC<CustomIconProps> = ({ iconName, className }) => {
  const iconUrl = `/Icons/bayada-icons.svg#${iconName}`;

  return (
    <svg className={`svg-icon ${className}`}>
      <use xlinkHref={iconUrl}></use>
    </svg>
  );
};

export default Icon;
