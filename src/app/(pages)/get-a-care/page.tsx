import React from 'react';
import type { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Get a care',
  description: 'Get a care page description',
};

const Page = () => {
  return <div>get a care page</div>;
};

export default Page;
