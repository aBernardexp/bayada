import React from 'react';
import type { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Explore Careers',
  description: 'Explore Careers page description',
};

const page = () => {
  return <div>Explore Careers page</div>;
};

export default page;
