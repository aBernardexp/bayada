import React from 'react';
import type { Metadata } from 'next';
import HomePage from '@/app/components/Pages/Home/Index';

export const metadata: Metadata = {
  title: 'Home',
  description: 'Home page description',
};
const page = () => {
  return <HomePage />;
};

export default page;
