'use client';

import { client } from '../../../config/client';
import React, { useEffect, useState } from 'react';
import Banner from './banner';

const HomePage = () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [bannerState, setBannerState] = useState<any>([]);
  useEffect(() => {
    getHomePage();
    return () => {
      // second
    };
  }, []);

  const getHomePage = async () => {
    try {
      console.log(process.env, 'accessToken');
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const res: any = await client?.getEntries({
        content_type: 'page',
        include: 10,
      });
      console.log(res.items, 'res from contentful');
      // setBannerState(res.items);
      setBannerState(res?.items[0]?.fields?.sections[0]?.fields);
    } catch (error) {
      // console.log(error, 'getHomePage');
    }
  };

  return (
    <div>
      <Banner bannerData={bannerState} />
    </div>
  );
};

export default HomePage;
