/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react';
import './banner.css';

const Banner = (props: any) => {
  const { bannerData } = props;
  console.log(bannerData?.Media?.fields?.file?.url);

  return (
    <div className='banner-container'>
      <div
        className='banner-wrapper '
        style={{
          backgroundImage: ` url(${bannerData?.Media?.fields?.file?.url})`,
        }}
      >
        <div className='left-side-banner'>
          <div className='banner-heading  font-frutiger'>
            {bannerData?.TextGroup?.fields?.title}
          </div>

          <div>
            <span className='bannner-sub-heading font-frutiger'>
              {bannerData?.TextGroup?.fields?.description}
            </span>
            <p className='banner-content font-frutiger'>
              {bannerData?.TextGroup?.fields?.paragraph}
            </p>
            <div className='input-holder'>
              <span>
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  width='24'
                  height='25'
                  viewBox='0 0 24 25'
                  fill='none'
                  className='searchIcon'
                >
                  <path
                    d='M19.0158 20.5496L13.2619 14.7958C12.7619 15.2086 12.1869 15.5316 11.5369 15.765C10.8869 15.9983 10.2145 16.115 9.51965 16.115C7.81048 16.115 6.36396 15.5232 5.1801 14.3396C3.99623 13.1561 3.4043 11.7099 3.4043 10.0012C3.4043 8.29247 3.99608 6.84579 5.17965 5.66118C6.36321 4.47658 7.80936 3.88428 9.5181 3.88428C11.2268 3.88428 12.6735 4.47621 13.8581 5.66008C15.0427 6.84395 15.635 8.29046 15.635 9.99963C15.635 10.7137 15.5151 11.3958 15.2754 12.0458C15.0356 12.6958 14.7158 13.2612 14.3158 13.7419L20.0946 19.5208C20.2331 19.6566 20.3023 19.8232 20.3023 20.0207C20.3023 20.2182 20.2241 20.3952 20.0675 20.5517C19.9241 20.6952 19.7491 20.7669 19.5427 20.7669C19.3363 20.7669 19.1607 20.6945 19.0158 20.5496ZM9.51965 14.615C10.8081 14.615 11.8995 14.1679 12.7937 13.2737C13.6879 12.3794 14.135 11.2881 14.135 9.99963C14.135 8.71116 13.6879 7.61981 12.7937 6.72558C11.8995 5.83134 10.8081 5.38423 9.51965 5.38423C8.23118 5.38423 7.13983 5.83134 6.2456 6.72558C5.35138 7.61981 4.90427 8.71116 4.90427 9.99963C4.90427 11.2881 5.35138 12.3794 6.2456 13.2737C7.13983 14.1679 8.23118 14.615 9.51965 14.615Z'
                    fill='#B9B9BC'
                  />
                </svg>
              </span>

              <input
                className='banner-input font-frutiger'
                type='text'
                name='firstname'
                placeholder='Tell us what you are looking for'
              ></input>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;
