/* eslint-disable  @typescript-eslint/no-explicit-any */

import { createTheme } from '@mui/material/styles';
import style from '../assets/scss/abstracts/_breakpoints.module.scss';
import colors from '../assets/scss/abstracts/_palette.module.scss';

declare module '@mui/material/styles' {
  interface BreakpointOverrides {
    xs: true;
    sm: true;
    md: true;
    lg: true;
    xl: true;
    xxl: true;
    xxxl: true;
  }
}

interface Ibreakpoint {
  xs: number;
  sm: number;
  md: number;
  lg: number;
  xl: number;
  xxl: number;
  xxxl: number;
}

const styleBreakPoints: Ibreakpoint = {
  xs: 0,
  sm: 0,
  md: 0,
  lg: 0,
  xl: 0,
  xxl: 0,
  xxxl: 0,
};

if (style) {
  for (const key in style) {
    (styleBreakPoints as any)[key] = Number(style[key]);
  }
}

const theme = createTheme({
  palette: {
    primary: {
      main: colors ? colors['ba-primary-red'] : '#CE0E2D',
    },
    secondary: {
      main: colors ? colors['ba-primary-black'] : '#232323',
    },
  },
  components: {
    MuiToolbar: {
      styleOverrides: {
        root: {
          backgroundColor: 'transparent',
        },
      },
    },
    MuiAppBar: {
      styleOverrides: {
        root: {
          backgroundColor: 'transparent',
          '&.MuiAppBar-colorPrimary': {
            color: 'var(--ba-primary-black)',
          },
        },
      },
    },
    MuiContainer: {
      defaultProps: {
        maxWidth: false,
      },
    },
    MuiButton: {
      variants: [
        {
          props: {
            variant: 'contained',
            color: 'primary',
          },
          style: {
            transition: 'all 0.3s ease-in-out',
            background:
              'linear-gradient(96deg, var(--ba-primary-red) 100%, var(--ba-primary-red) 100%, var(--ba-primary-red) 100%, var(--ba-primary-red) 100%)',
            '&:hover': {
              background:
                'linear-gradient(96deg, var(--ba-primary-600) 0%, var(--ba-primary-700) 19.27%, var(--ba-primary-700) 66.15%, var(--ba-primary-800) 100%)',
            },
            '&:focus': {
              background: colors ? colors['ba-btn-focus-bg'] : '#AE0420',
            },
            textTransform: 'none',
            padding: '0.813rem 1.5rem',
          },
        },
        {
          props: { variant: 'contained', color: 'primary', disabled: true },
          style: {
            transition: 'all 0.3s ease-in-out',
            background: colors ? colors['ba-btn-disabled'] : '#C7C7C7',
            textTransform: 'none',
            padding: '0.813rem 1.5rem',
            pointerEvents: 'none',
          },
        },
        {
          props: {
            variant: 'contained',
            color: 'secondary',
          },
          style: {
            transition: 'all 0.3s ease-in-out',
            textTransform: 'none',
            padding: '0.813rem 1.5rem',
            '&:hover': {
              background: colors ? colors['ba-primary-black'] : '#232323',
              color: '#ffffff',
            },
          },
        },
        {
          props: {
            variant: 'outlined',
            color: 'secondary',
          },
          style: {
            textTransform: 'none',
            padding: '0.813rem 1.5rem',
            transition: 'all 0.3s ease-in-out',
            '&:hover': {
              color: '#ffffff',
              background: colors ? colors['ba-primary-black'] : '#232323',
            },
            '&:focus': {
              background: '#000000',
              color: '#ffffff',
            },
          },
        },
      ],
    },
    MuiIconButton: {
      variants: [
        {
          props: { color: 'primary' },
          style: {
            transition: 'background 0.3s ease-in-out',
            background:
              'linear-gradient(96deg, var(--ba-primary-red) 100%, var(--ba-primary-red) 100%, var(--ba-primary-red) 100%, var(--ba-primary-red) 100%)',
            '&:hover': {
              background:
                'linear-gradient(96deg, var(--ba-primary-600) 0%, var(--ba-primary-700) 19.27%, var(--ba-primary-700) 66.15%, var(--ba-primary-800) 100%)',
            },
            '&:focus': {
              background: colors ? colors['ba-btn-focus-bg'] : '#AE0420',
            },
            textTransform: 'none',
            padding: '0.813rem 1.5rem',
          },
        },
      ],
    },
    MuiTextField: {
      variants: [
        {
          props: {},
          style: {},
        },
      ],
      styleOverrides: {
        root: {
          '&:hover': {
            backgroundColor: colors ? colors['ba-field-hover-bg'] : '#F3F3F3',
          },
          '&:focus': {
            backgroundColor: colors ? colors['ba-field-focus-bg'] : '#EAEAEA',
          },
          backgroundColor: '#ffffff',
        },
      },
    },
  },
  typography: {
    fontFamily: ["'Inter', sans-serif", "'frutiger', sans-serif"].join(','),
    fontSize: 16,
  },
  breakpoints: {
    values: styleBreakPoints as Ibreakpoint,
  },
});

export default theme;
